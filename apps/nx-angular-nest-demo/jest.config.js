module.exports = {
  name: 'nx-angular-nest-demo',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/nx-angular-nest-demo',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
